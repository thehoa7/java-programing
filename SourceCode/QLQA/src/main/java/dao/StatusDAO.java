package dao;

import commondao.AbstractDao;
import commondao.DataAccessLayerException;
import entities.Employee;
import entities.Status;

import java.util.List;

public class StatusDAO extends AbstractDao {
	public StatusDAO() {
        super();
    }
	
	/**
     * Insert a new Event into the database.
     * @param status
     */
    public void create(Status status) throws DataAccessLayerException {
        super.saveOrUpdate(status);
    }


    /**
     * Delete a detached Event from the database.
     * @param status
     */
    public void delete(Status status) throws DataAccessLayerException {
        super.delete(status);
    }

    /**
     * Find an Event by its primary key.
     * @param id
     * @return
     */
    public Status find(Long id) throws DataAccessLayerException {
        return (Status) super.find(Status.class, id);
    }

    /**
     * Updates the state of a detached Event.
     *
     * @param event
     */
    public void update(Status status) throws DataAccessLayerException {
        super.saveOrUpdate(status);
    }

    /**
     * Finds all Events in the database.
     * @return
     */
    @SuppressWarnings("rawtypes")
	public List findAll() throws DataAccessLayerException{
        return super.findAll(Status.class);
    }

}
