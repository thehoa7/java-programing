package entities;

// Generated May 7, 2016 9:19:46 AM by Hibernate Tools 3.4.0.CR1

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Category generated by hbm2java
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "category", catalog = "qlqa")
public class Category implements java.io.Serializable {

	private Integer id;
	private Status status;
	private String name;
	private Set<Food> foods = new HashSet<Food>(0);

	public Category() {
	}

	public Category(Status status) {
		this.status = status;
	}

	public Category(Status status, String name, Set<Food> foods) {
		this.status = status;
		this.name = name;
		this.foods = foods;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "Id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "StatusId", nullable = false)
	public Status getStatus() {
		return this.status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	@Column(name = "Name", length = 100)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "category")
	public Set<Food> getFoods() {
		return this.foods;
	}

	public void setFoods(Set<Food> foods) {
		this.foods = foods;
	}

}
