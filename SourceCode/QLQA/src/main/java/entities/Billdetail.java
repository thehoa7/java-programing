package entities;

// Generated May 7, 2016 9:19:46 AM by Hibernate Tools 3.4.0.CR1

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Billdetail generated by hbm2java
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "billdetail", catalog = "qlqa")
public class Billdetail implements java.io.Serializable {

	private int id;
	private Food food;
	private Bill bill;
	private Status status;
	private boolean quantum;
	private String description;

	public Billdetail() {
	}

	public Billdetail(int id, Food food, Bill bill, Status status,
			boolean quantum) {
		this.id = id;
		this.food = food;
		this.bill = bill;
		this.status = status;
		this.quantum = quantum;
	}

	public Billdetail(int id, Food food, Bill bill, Status status,
			boolean quantum, String description) {
		this.id = id;
		this.food = food;
		this.bill = bill;
		this.status = status;
		this.quantum = quantum;
		this.description = description;
	}

	@Id
	@Column(name = "Id", unique = true, nullable = false)
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "FoodId", nullable = false)
	public Food getFood() {
		return this.food;
	}

	public void setFood(Food food) {
		this.food = food;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "BillId", nullable = false)
	public Bill getBill() {
		return this.bill;
	}

	public void setBill(Bill bill) {
		this.bill = bill;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "StatusId", nullable = false)
	public Status getStatus() {
		return this.status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	@Column(name = "Quantum", nullable = false)
	public boolean isQuantum() {
		return this.quantum;
	}

	public void setQuantum(boolean quantum) {
		this.quantum = quantum;
	}

	@Column(name = "Description")
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
