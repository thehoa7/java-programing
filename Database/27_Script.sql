-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema qlqa
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema qlqa
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `qlqa` DEFAULT CHARACTER SET utf8 ;
USE `qlqa` ;

-- -----------------------------------------------------
-- Table `qlqa`.`computerproficiency`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `qlqa`.`computerproficiency` (
  `Id` TINYINT(1) NOT NULL AUTO_INCREMENT,
  `Name` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`Id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `qlqa`.`role`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `qlqa`.`role` (
  `Id` INT(11) NOT NULL AUTO_INCREMENT,
  `Name` VARCHAR(100) NULL DEFAULT NULL,
  `Type` VARCHAR(45) NULL DEFAULT NULL,
  `Rolecol` VARCHAR(45) NULL DEFAULT NULL,
  `Interns_Id` INT(11) NOT NULL,
  PRIMARY KEY (`Id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `qlqa`.`status`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `qlqa`.`status` (
  `Id` INT(11) NOT NULL AUTO_INCREMENT,
  `Name` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`Id`))
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `qlqa`.`employee`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `qlqa`.`employee` (
  `Id` INT(11) NOT NULL AUTO_INCREMENT,
  `FullName` VARCHAR(100) NULL DEFAULT NULL,
  `Phone` VARCHAR(15) NULL DEFAULT NULL,
  `Email` VARCHAR(45) NULL DEFAULT NULL,
  `Password` VARCHAR(255) NULL DEFAULT NULL,
  `Employeecol` TINYINT(1) NULL DEFAULT NULL,
  `Birthday` DATETIME NULL DEFAULT NULL,
  `Address` VARCHAR(255) NULL DEFAULT NULL,
  `IdentityNumber` VARCHAR(15) NULL DEFAULT NULL,
  `RoleId` INT(11) NOT NULL,
  `IsInterns` TINYINT(1) NULL DEFAULT NULL,
  `StartDate` DATETIME NULL DEFAULT NULL,
  `ComputerProficiencyId` TINYINT(1) NOT NULL,
  `StatusId` INT(11) NOT NULL,
  PRIMARY KEY (`Id`),
  INDEX `fk_Employee_Role_idx` (`RoleId` ASC),
  INDEX `fk_Employee_ComputerProficiency1_idx` (`ComputerProficiencyId` ASC),
  INDEX `fk_Employee_Status1_idx` (`StatusId` ASC),
  CONSTRAINT `fk_Employee_ComputerProficiency1`
    FOREIGN KEY (`ComputerProficiencyId`)
    REFERENCES `qlqa`.`computerproficiency` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Employee_Role`
    FOREIGN KEY (`RoleId`)
    REFERENCES `qlqa`.`role` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Employee_Status1`
    FOREIGN KEY (`StatusId`)
    REFERENCES `qlqa`.`status` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `qlqa`.`stable`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `qlqa`.`stable` (
  `Id` INT(11) NOT NULL AUTO_INCREMENT,
  `Name` VARCHAR(100) NULL DEFAULT NULL,
  `StatusId` INT(11) NOT NULL,
  PRIMARY KEY (`Id`),
  INDEX `fk_Table_Status1_idx` (`StatusId` ASC),
  CONSTRAINT `fk_Table_Status1`
    FOREIGN KEY (`StatusId`)
    REFERENCES `qlqa`.`status` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `qlqa`.`bill`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `qlqa`.`bill` (
  `Id` INT(11) NOT NULL AUTO_INCREMENT,
  `EmployeeId` INT(11) NOT NULL,
  `TableId` INT(11) NOT NULL,
  `CreatedAt` DATETIME NULL DEFAULT NULL,
  `NumberOfCustomer` TINYINT(1) NULL DEFAULT NULL,
  `StatusId` INT(11) NOT NULL,
  `Amount` DOUBLE NULL DEFAULT NULL,
  `CustomerPay` DOUBLE NULL DEFAULT NULL,
  `CustomerReturn` DOUBLE NULL DEFAULT NULL,
  `MergeBillId` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`Id`),
  INDEX `fk_Bill_Employee1_idx` (`EmployeeId` ASC),
  INDEX `fk_Bill_Table1_idx` (`TableId` ASC),
  INDEX `fk_Bill_Status1_idx` (`StatusId` ASC),
  CONSTRAINT `fk_Bill_Employee1`
    FOREIGN KEY (`EmployeeId`)
    REFERENCES `qlqa`.`employee` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Bill_Status1`
    FOREIGN KEY (`StatusId`)
    REFERENCES `qlqa`.`status` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Bill_Table1`
    FOREIGN KEY (`TableId`)
    REFERENCES `qlqa`.`stable` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `qlqa`.`category`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `qlqa`.`category` (
  `Id` INT(11) NOT NULL AUTO_INCREMENT,
  `Name` VARCHAR(100) NULL DEFAULT NULL,
  `StatusId` INT(11) NOT NULL,
  PRIMARY KEY (`Id`),
  INDEX `fk_Category_Status1_idx` (`StatusId` ASC),
  CONSTRAINT `fk_Category_Status1`
    FOREIGN KEY (`StatusId`)
    REFERENCES `qlqa`.`status` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `qlqa`.`food`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `qlqa`.`food` (
  `Id` INT(11) NOT NULL AUTO_INCREMENT,
  `Name` VARCHAR(255) NULL DEFAULT NULL,
  `Price` DOUBLE NULL DEFAULT NULL,
  `CategoryId` INT(11) NOT NULL,
  `StatusId` INT(11) NOT NULL,
  PRIMARY KEY (`Id`),
  INDEX `fk_Food_Category1_idx` (`CategoryId` ASC),
  INDEX `fk_Food_Status1_idx` (`StatusId` ASC),
  CONSTRAINT `fk_Food_Category1`
    FOREIGN KEY (`CategoryId`)
    REFERENCES `qlqa`.`category` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Food_Status1`
    FOREIGN KEY (`StatusId`)
    REFERENCES `qlqa`.`status` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `qlqa`.`billdetail`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `qlqa`.`billdetail` (
  `Id` INT(11) NOT NULL AUTO_INCREMENT,
  `FoodId` INT(11) NOT NULL,
  `StatusId` INT(11) NOT NULL,
  `BillId` INT(11) NOT NULL,
  `Quantum` TINYINT(1) NOT NULL DEFAULT '1',
  `Description` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`Id`),
  INDEX `fk_BillDetail_Food1_idx` (`FoodId` ASC),
  INDEX `fk_BillDetail_Status1_idx` (`StatusId` ASC),
  INDEX `fk_BillDetail_Bill1_idx` (`BillId` ASC),
  CONSTRAINT `fk_BillDetail_Bill1`
    FOREIGN KEY (`BillId`)
    REFERENCES `qlqa`.`bill` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_BillDetail_Food1`
    FOREIGN KEY (`FoodId`)
    REFERENCES `qlqa`.`food` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_BillDetail_Status1`
    FOREIGN KEY (`StatusId`)
    REFERENCES `qlqa`.`status` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `qlqa`.`item`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `qlqa`.`item` (
  `Id` TINYINT(4) NOT NULL AUTO_INCREMENT,
  `Name` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`Id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `qlqa`.`employeeitems`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `qlqa`.`employeeitems` (
  `Id` INT(11) NOT NULL AUTO_INCREMENT,
  `EmployeeId` INT(11) NOT NULL,
  `ItemId` TINYINT(4) NOT NULL,
  `StatusId` INT(11) NOT NULL,
  PRIMARY KEY (`Id`),
  INDEX `fk_Employee_has_Item_Item1_idx` (`ItemId` ASC),
  INDEX `fk_Employee_has_Item_Employee1_idx` (`EmployeeId` ASC),
  INDEX `fk_Employee_has_Item_Status1_idx` (`StatusId` ASC),
  CONSTRAINT `fk_Employee_has_Item_Employee1`
    FOREIGN KEY (`EmployeeId`)
    REFERENCES `qlqa`.`employee` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Employee_has_Item_Item1`
    FOREIGN KEY (`ItemId`)
    REFERENCES `qlqa`.`item` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Employee_has_Item_Status1`
    FOREIGN KEY (`StatusId`)
    REFERENCES `qlqa`.`status` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `qlqa`.`menu`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `qlqa`.`menu` (
  `Id` INT(11) NOT NULL AUTO_INCREMENT,
  `Name` VARCHAR(255) NULL DEFAULT NULL,
  `CreatedAt` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`Id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `qlqa`.`menufood`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `qlqa`.`menufood` (
  `Id` INT(11) NOT NULL AUTO_INCREMENT,
  `MenuId` INT(11) NOT NULL,
  `FoodId` INT(11) NOT NULL,
  PRIMARY KEY (`Id`),
  INDEX `fk_Menu_has_Food_Food1_idx` (`FoodId` ASC),
  INDEX `fk_Menu_has_Food_Menu1_idx` (`MenuId` ASC),
  CONSTRAINT `fk_Menu_has_Food_Food1`
    FOREIGN KEY (`FoodId`)
    REFERENCES `qlqa`.`food` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Menu_has_Food_Menu1`
    FOREIGN KEY (`MenuId`)
    REFERENCES `qlqa`.`menu` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `qlqa`.`permission`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `qlqa`.`permission` (
  `Id` TINYINT(1) NOT NULL AUTO_INCREMENT,
  `Name` VARCHAR(100) NULL DEFAULT NULL,
  PRIMARY KEY (`Id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `qlqa`.`rolepermissions`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `qlqa`.`rolepermissions` (
  `Id` INT(11) NOT NULL AUTO_INCREMENT,
  `RoleId` INT(11) NOT NULL,
  `PermissionId` TINYINT(1) NOT NULL,
  `IsEnabled` TINYINT(1) NULL DEFAULT NULL,
  PRIMARY KEY (`Id`),
  INDEX `fk_Role_has_Permission_Permission1_idx` (`PermissionId` ASC),
  INDEX `fk_Role_has_Permission_Role1_idx` (`RoleId` ASC),
  CONSTRAINT `fk_Role_has_Permission_Permission1`
    FOREIGN KEY (`PermissionId`)
    REFERENCES `qlqa`.`permission` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Role_has_Permission_Role1`
    FOREIGN KEY (`RoleId`)
    REFERENCES `qlqa`.`role` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `qlqa`.`shift`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `qlqa`.`shift` (
  `Id` TINYINT(1) NOT NULL AUTO_INCREMENT,
  `Name` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`Id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `qlqa`.`schedule`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `qlqa`.`schedule` (
  `Id` INT(11) NOT NULL AUTO_INCREMENT,
  `EmployeeId` INT(11) NOT NULL,
  `ShiftId` TINYINT(1) NOT NULL,
  `WorkingDate` DATETIME NULL DEFAULT NULL,
  `StatusId` INT(11) NOT NULL,
  `IsWorked` TINYINT(1) NULL DEFAULT NULL,
  `MinutesLate` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`Id`),
  INDEX `fk_Employee_has_Shift_Shift1_idx` (`ShiftId` ASC),
  INDEX `fk_Employee_has_Shift_Employee1_idx` (`EmployeeId` ASC),
  INDEX `fk_Schedule_Status1_idx` (`StatusId` ASC),
  CONSTRAINT `fk_Employee_has_Shift_Employee1`
    FOREIGN KEY (`EmployeeId`)
    REFERENCES `qlqa`.`employee` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Employee_has_Shift_Shift1`
    FOREIGN KEY (`ShiftId`)
    REFERENCES `qlqa`.`shift` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Schedule_Status1`
    FOREIGN KEY (`StatusId`)
    REFERENCES `qlqa`.`status` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `qlqa`.`shiftnumbermember`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `qlqa`.`shiftnumbermember` (
  `Id` INT(11) NOT NULL AUTO_INCREMENT,
  `RoleId` INT(11) NOT NULL,
  `ShiftId` TINYINT(1) NOT NULL,
  `NumberEmployee` TINYINT(1) NULL DEFAULT NULL,
  PRIMARY KEY (`Id`),
  INDEX `fk_Shift_has_Role_Role1_idx` (`RoleId` ASC),
  INDEX `fk_Shift_has_Role_Shift1_idx` (`ShiftId` ASC),
  CONSTRAINT `fk_Shift_has_Role_Role1`
    FOREIGN KEY (`RoleId`)
    REFERENCES `qlqa`.`role` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Shift_has_Role_Shift1`
    FOREIGN KEY (`ShiftId`)
    REFERENCES `qlqa`.`shift` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
